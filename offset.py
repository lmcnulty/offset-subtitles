#!/usr/bin/env python3

import sys

def pad(s, n=2):
	for i in range(n - len(s)):
		s = "0" + s
	return s

def offset_time(timestring, offset):
	offset = int(offset)
	s = timestring.split(":")
	s1 = s[2].split(",")
	hours = int(s[0])
	minutes = int(s[1])
	seconds = int(s1[0])
	millis = int(s1[1])

	seconds += offset
	if seconds > 59:
		seconds -= 60
		minutes += 1
	if minutes > 59:
		minutes -= 60
		hours += 1
	
	return (  pad(str(hours))   + ":" 
	        + pad(str(minutes)) + ":" 
	        + pad(str(seconds)) + "," 
	        + pad(str(millis), 3)    )


if __name__ == "__main__":
	if len(sys.argv) < 3:
		print("Usage: offset.py <file> <offset-in-seconds>")
		exit()

	try:
		with open(sys.argv[1]) as f:
			text = f.read()
	except:
		print("Error: Can't open '" + sys.argv[2] +"' for reading.")
		exit()

	try:
		offset = int(sys.argv[2])
	except:
		print("Error: Offset must be an integer.")
		exit()

	for line in text.split("\n"):
		if len(line) > 0 and line[0] == "0":
			a = line.split(" --> ")
			print(offset_time(a[0], offset), end="")
			print(" --> ", end="")
			print(offset_time(a[1], offset))
		else:
			print(line)
